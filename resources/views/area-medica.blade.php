@extends('layout')

@section('content')
<section class="banner-servicos">
    <div class="container">
        <div class="sobreHospital areaMedica bg-alternative">
            <h1>CADASTRO MÉDICO</h1>
            <p>Para fazer parte do nosso corpo clínico ou poder utilizar nossas dependências para exercício da função, preencha o cadastro abaixo e siga as orientações descritas.</p>
            <a href="{{ route('index.cadastro-medico') }}" title="Clique para fazer o cadastro médico" class="link">CADASTRE-SE</a>
        </div>
    </div>
    <div class="serivcos-img-wrapper">
        <img src="images/banner-corpo-clinico.png" alt="Banner da página de corpo clínico">
    </div>
</section>
<section class="hospital areaMedica">
    <div class="wrap">
        <div class="exames areaMedica">
            <h2>PROGRAMA DE RESIDÊNCIA</h2>
            <p>O programa de Residência em Terapia Intensiva é realizado através de concurso programado para todo início de ano. Anualmente, dois candidatos são aprovados para o programa, com duração de 2 anos.</p>
        </div>
    </div>
</section>
@endsection