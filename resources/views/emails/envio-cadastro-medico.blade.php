<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>Hospitais Next</title>
</head>
<body>
    <table>
        <thead>
            <h1 style="font: 30px tahoma, sans-serif; color: #265F98; margin: 0 0 20px 0;">{{ $heads['sb'] . ' - ' . $heads['co'] }}</h1>
        </thead>
        <tbody>
            <ul style="list-style: none;">
                <?php $c = 0; ?>
                @foreach($fields as $key => $field)
                    <li style="padding: 20px 15px; margin: 0 0 20px 0; border-bottom: 1px solid #cccccc; width: 450px; float: left; background-color: {{ (($c % 2 == 0) ? '#eeeeee' : '#ffffff') }};">
                        <h5 style="font: 16px tahoma, sans-serif; color: #00A79B; margin: 0 0 5px 0; text-transform: uppercase;">{{ $key }}:</h5>
                        <p style="font: 16px tahoma, sans-serif; color: #505050; margin: 5px 0 0 0;">{{ $field }}</p>
                    </li>
                    <?php $c++; ?>
                @endforeach
            </ul>
        </tbody>
    </table>
</body>
</html>