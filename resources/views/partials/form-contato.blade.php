<form action="{{ route('index.envio-contato') }}" method="post" accept-charset="utf-8" enctype="application/x-www-form-urlencoded">

    <fieldset>
        <input type="text" name="nome" value="" placeholder="NOME">
    </fieldset>
    <fieldset>
        <input type="email" name="email" value="" placeholder="E-MAIL">
    </fieldset>
    <fieldset>
        <input type="text" name="assunto" value="" placeholder="ASSUNTO" maxlength="140">
    </fieldset>
    <fieldset>
        <textarea name="texto" placeholder="TEXTO"></textarea>
    </fieldset>
    <fieldset>
        <div class="alert">
            <ul></ul>
        </div>
    </fieldset>
    <fieldset>
        <button>Enviar</button>
    </fieldset>
</form>