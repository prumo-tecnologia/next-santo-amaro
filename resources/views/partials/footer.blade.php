<div class="wrap">
    <div class="contato">
        <div>
            <p class="titulo">Central de Atendimento:</p>
            <p class="info">(11) 2185-0500</p>
        </div>
        <div>
            <p class="titulo">SAC:</p>
            <p class="info marginBottom">(11) 2185-0587</p>
        </div>
        <div>
            <p class="titulo">E-mail SAC:</p>
            <p class="info marginBottom"><a href="mailto:sachmo@amil.com.br">sachmo@amil.com.br</a></p>
        </div>
    </div>
    <nav>
        <ul>
            <li><a name="rodape" id="rodape" href="{{ route('index.estrutura') }}" title="ESTRUTURA" class="{{ (($uri == 'estrutura') ? 'active' : '') }}">ESTRUTURA</a></li>
            <li><a href="{{ route('index.servicos') }}" title="SERVIÇOS" class="{{ (($uri == 'servicos') ? 'active' : '') }}">SERVIÇOS</a></li>
            <li><a href="{{ route('index.imprensa') }}" title="IMPRENSA" class="{{ (($uri == 'imprensa') ? 'active' : '') }}">IMPRENSA</a></li>
            <!--<li><a href="{{ route('index.convenio') }}" title="CONVÊNIO" class="{{ (($uri == 'convenio') ? 'active' : '') }}">Convênios</a></li>-->
            <li><a href="{{ route('index.contato') }}" title="CONTATO" class="{{ (($uri == 'contato') ? 'active' : '') }}">CONTATO</a></li>
        </ul>
    </nav>
</div>
<div class="assinatura">
    <p>Responsável Técnico: Dr. Antônio Eduardo Giriboni Monteiro - CRM 78419</p>
    <p><a href="https://institucional.amil.com.br/etica-compliance" target="_blank" style="text-decoration: underline;"> Ética e Compliance</a> 
    | <a href="https://institucional.amil.com.br/comunicado-de-privacidade-da-amil" target="_blank" style="text-decoration: underline;">Comunicado de privacidade</a> 
    | <a href="https://institucional.amil.com.br/politica-de-uso-de-cookies" target="_blank" style="text-decoration: underline;">Política de Cookies</a> 
    - Hospitais Next - Marca Registrada. Todos os direitos reservados. <a href="http://www.casademarcas.com.br/" title="Casa de Marcas" target="_blank">Desenvolvido por Casa de Marcas</a></p>
</div>