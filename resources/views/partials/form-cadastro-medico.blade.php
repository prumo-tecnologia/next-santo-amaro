<form accept-charset="utf-8">
    <fieldset>
        <input type="text" name="nome" value="" placeholder="NOME">
        <input type="text" name="crm" value="" placeholder="CRM">
    </fieldset>
    <fieldset>
        <input type="text" name="nascimento" value="" placeholder="DATA DE NASCIMENTO">
        <input type="text" name="civil" value="" placeholder="ESTADO CIVIL">
    </fieldset>
    <fieldset>
        <input type="text" name="rg" value="" placeholder="RG">
        <input type="text" name="cpf" value="" placeholder="CPF">
    </fieldset>
    <fieldset>
        <div>
            <label>GÊNERO</label>
            <input type="radio" name="genero" value="masculino" placeholder="">
            <small>Masculino</small>
            <input type="radio" name="genero" value="feminino" placeholder="">
            <small>Feminino</small>
        </div>
        <div>
            <input type="text" name="nacionalidade" value="" placeholder="NACIONALIDADE">
        </div>
    </fieldset>
    <fieldset>
        <input type="text" name="naturalidade" value="" placeholder="NATURALIDADE">
        <input type="text" name="residencial" value="" placeholder="ENDEREÇO RESIDENCIAL">
    </fieldset>
    <fieldset>
        <input type="text" name="residencial_bairro" value="" placeholder="BAIRRO">
        <input type="text" name="residencial_cep" value="" placeholder="CEP">
    </fieldset>
    <fieldset>
        <input type="text" name="residencial_cidade" value="" placeholder="CIDADE">
        <input type="text" name="residencial_estado" value="" placeholder="ESTADO">
    </fieldset>
    <fieldset>
        <input type="text" name="consultorio" value="" placeholder="ENDEREÇO CONSULTÓRIO">
        <input type="text" name="consultorio_bairro" value="" placeholder="BAIRRO">
    </fieldset>
    <fieldset>
        <input type="text" name="consultorio_cep" value="" placeholder="CEP">
        <input type="text" name="consultorio_cidade" value="" placeholder="CIDADE">
    </fieldset>
    <fieldset>
        <input type="text" name="consultorio_estado" value="" placeholder="ESTADO">
        <input type="tel" name="tel_residencial" value="" placeholder="TELEFONE RESIDENCIAL">
    </fieldset>
    <fieldset>
        <input type="tel" name="tel_consultorio" value="" placeholder="TELEFONE CONSULTÓRIO">
        <input type="tel" name="tel_celular" value="" placeholder="TELEFONE CELULAR">
    </fieldset>
    <fieldset>
        <input type="email" name="email" value="" placeholder="E-MAIL">
        <input type="text" name="idioma" value="" placeholder="IDIOMA FALADO PELO PROFISSIONAL">
    </fieldset>
    <fieldset>
        <input type="text" name="outro_idioma" value="" placeholder="OUTRO IDIOMA FALADO PELO PROFISSIONAL">
        <input type="text" name="especialidade" value="" placeholder="ESPECIALIDADE">
    </fieldset>
    <fieldset>
        <input type="text" name="outra_especialidade" value="" placeholder="OUTRA ESPECIALIDADE">
        <input type="text" name="area_atuacao" value="" placeholder="ÁREA DE ATUAÇÃO">
    </fieldset>
    <fieldset>
        <div>
            <input type="text" name="dados_carro" value="" placeholder="DADOS DO CARRO">
        </div>
        <div>
            <input type="text" name="chefe_equipe" value="" placeholder="CHEFE DA EQUIPE">
            <small class="obs">(caso a atuação não seja em equipe própria)</small>
        </div>
    </fieldset>
    <fieldset>
        <div>
            <label>ATUAÇÃO SERÁ EM EQUIPE PRÓPRIA?</label>
            <input type="radio" name="equipe" value="sim" placeholder="">
            <small>SIM</small>
            <input type="radio" name="equipe" value="não" placeholder="">
            <small>NÃO</small>
        </div>
    </fieldset>
    <fieldset>
        <h2>FORMAÇÃO PROFISSIONAL</h2>
    </fieldset>
    <fieldset>
        <input type="text" name="formacao_superior" value="" placeholder="CURSO SUPERIOR">
        <input type="text" name="formacao_estado" value="" placeholder="ESTADO">
    </fieldset>
    <fieldset>
        <input type="text" name="formacao_conclusao" value="" placeholder="ANO DA CONCLUSÃO">
        <input type="text" name="formacao_residencia" value="" placeholder="RESIDÊNCIA MÉDICA">
    </fieldset>
    <fieldset>
        <input type="text" name="formacao_residencia_estado" value="" placeholder="ESTADO">
        <input type="text" name="formacao_residencia_conclusao" value="" placeholder="ANO DA CONCLUSÃO">
    </fieldset>
    <fieldset>
        <div class="full">
            <label>PÓS-GRADUÇÃO?</label>
            <input type="checkbox" name="pos_lato_sensu" value="sim" placeholder="">
            <small>LATO SENSU</small>
            <input type="checkbox" name="pos_mestrado" value="sim" placeholder="">
            <small>MESTRADO</small>
            <input type="checkbox" name="pos_doutorado" value="sim" placeholder="">
            <small>DOUTORADO</small>
            <input type="checkbox" name="pos_livre_docencia" value="sim" placeholder="">
            <small>LIVRE DOCÊNCIA</small>
        </div>
    </fieldset>
    <fieldset>
        <input type="text" name="pos_instituicao" value="" placeholder="INSTITUIÇÃO">
        <input type="text" name="pos_instituicao_estado" value="" placeholder="ESTADO">
    </fieldset>
    <fieldset>
        <input type="text" name="pos_instituicao_conclusao" value="" placeholder="ANO DA CONCLUSÃO">
    </fieldset>
    <fieldset>
        <h2>REFERÊNCIAS PROFISSIONAIS</h2>
    </fieldset>
    <fieldset>
        <div class="full">
            <small class="obs">
                • Contato de 2 (dois) profissionais que estejam familiarizados com sua prática profissional e possam fornecer referências sobre sua experiência. Caso você tenha terminado seu treinamento a menos de 3 anos, um dos nomes abaixo deverá ser o do chefe do serviço ou diretor do programa.
            </small>
            <small class="obs">
                • Contato de 2 (dois) profissionais, de preferência da sua área, que estejam cadastrados no corpo clinico desse Hospital há pelo menos 3 anos.
            </small>
            <small class="obs">
                • Contato de chefe de serviço/diretor clinico de 1 hospital no qual você se encontrava cadastrado com prerrogativa clínica.
            </small>
        </div>
    </fieldset>
    <fieldset>
        <input type="text" name="ref1_nome" value="" placeholder="NOME COMPLETO">
        <input type="text" name="ref1_crm" value="" placeholder="CRM">
    </fieldset>
    <fieldset>
        <input type="text" name="ref1_instituicao" value="" placeholder="INSTITUIÇÃO">
        <input type="text" name="ref1_funcao" value="" placeholder="FUNÇÃO">
    </fieldset>
    <fieldset>
        <input type="text" name="ref1_especialidade" value="" placeholder="ESPECIALIDADE">
        <input type="email" name="ref1_email" value="" placeholder="E-MAIL">
    </fieldset>
    <fieldset class="margin">
        <input type="tel" name="ref1_tel_comercial" value="" placeholder="TELEFONE COMERCIAL">
        <input type="tel" name="ref1_tel_celular" value="" placeholder="TELEFONE CELULAR">
    </fieldset>
    <fieldset>
        <input type="text" name="ref2_nome" value="" placeholder="NOME COMPLETO">
        <input type="text" name="ref2_crm" value="" placeholder="CRM">
    </fieldset>
    <fieldset>
        <input type="text" name="ref2_instituicao" value="" placeholder="INSTITUIÇÃO">
        <input type="text" name="ref2_funcao" value="" placeholder="FUNÇÃO">
    </fieldset>
    <fieldset>
        <input type="text" name="ref2_especialidade" value="" placeholder="ESPECIALIDADES">
        <input type="email" name="ref2_email" value="" placeholder="E-MAIL">
    </fieldset>
    <fieldset class="margin">
        <input type="tel" name="ref2_tel_comercial" value="" placeholder="TELEFONE COMERCIAL">
        <input type="tel" name="ref2_tel_celular" value="" placeholder="TELEFONE CELULAR">
    </fieldset>
    <fieldset>
        <input type="text" name="ref3_nome" value="" placeholder="NOME COMPLETO">
        <input type="text" name="ref3_crm" value="" placeholder="CRM">
    </fieldset>
    <fieldset>
        <input type="text" name="ref3_instituicao" value="" placeholder="INSTITUIÇÃO">
        <input type="text" name="ref3_funcao" value="" placeholder="FUNÇÃO">
    </fieldset>
    <fieldset>
        <input type="text" name="ref3_especialidade" value="" placeholder="ESPECIALIDADE">
        <input type="email" name="ref3_email" value="" placeholder="E-MAIL">
    </fieldset>
    <fieldset class="margin">
        <input type="tel" name="ref3_tel_comercial" value="" placeholder="TELEFONE COMERCIAL">
        <input type="tel" name="ref3_tel_celular" value="" placeholder="TELEFONE CELULAR">
    </fieldset>
    <fieldset>
        <input type="text" name="ref4_nome" value="" placeholder="NOME COMPLETO">
        <input type="text" name="ref4_crm" value="" placeholder="CRM">
    </fieldset>
    <fieldset>
        <input type="text" name="ref4_instituicao" value="" placeholder="INSTITUIÇÃO">
        <input type="text" name="ref4_funcao" value="" placeholder="FUNÇÃO">
    </fieldset>
    <fieldset>
        <input type="text" name="ref4_especialidade" value="" placeholder="ESPECIALIDADES">
        <input type="email" name="ref4_email" value="" placeholder="E-MAIL">
    </fieldset>
    <fieldset class="margin">
        <input type="tel" name="ref4_tel_comercial" value="" placeholder="TELEFONE COMERCIAL">
        <input type="tel" name="ref4_tel_celular" value="" placeholder="TELEFONE CELULAR">
    </fieldset>
    <fieldset>
    <fieldset>
        <input type="text" name="ref5_nome" value="" placeholder="NOME COMPLETO">
        <input type="text" name="ref5_crm" value="" placeholder="CRM">
    </fieldset>
    <fieldset>
        <input type="text" name="ref5_instituicao" value="" placeholder="INSTITUIÇÃO">
        <input type="text" name="ref5_funcao" value="" placeholder="FUNÇÃO">
    </fieldset>
    <fieldset>
        <input type="text" name="ref5_especialidade" value="" placeholder="ESPECIALIDADE">
        <input type="email" name="ref5_email" value="" placeholder="E-MAIL">
    </fieldset>
    <fieldset class="margin">
        <input type="tel" name="ref5_tel_comercial" value="" placeholder="TELEFONE COMERCIAL">
        <input type="tel" name="ref5_tel_celular" value="" placeholder="TELEFONE CELULAR">
    </fieldset>
        <h2>LISTAR A(S) SOCIEDADE(S) PROFISSIONAL QUE É AFILIADO</h2>
    </fieldset>
    <fieldset>
        <div>
            <input type="text" name="sociedade_prof1" value="" placeholder="SOCIEDADE PROFISSIONAL">
        </div>
        <div>
            <label>MEMBRO ATUAL?</label>
            <input type="radio" name="sociedade_prof1_membro" value="sim" placeholder="">
            <small>SIM</small>
            <input type="radio" name="sociedade_prof1_membro" value="não" placeholder="">
            <small>NÃO</small>
        </div>
    </fieldset>
    <fieldset class="margin">
        <input type="text" name="sociedade_prof1_admissao" value="" placeholder="DATA DE ADMISSÃO">
    </fieldset>
    <fieldset>
        <div>
            <input type="text" name="sociedade_prof2" value="" placeholder="SOCIEDADE PROFISSIONAL">
        </div>
        <div>
            <label>MEMBRO ATUAL?</label>
            <input type="radio" name="sociedade_prof2_membro" value="sim" placeholder="">
            <small>SIM</small>
            <input type="radio" name="sociedade_prof2_membro" value="não" placeholder="">
            <small>NÃO</small>
        </div>
    </fieldset>
    <fieldset class="margin">
        <input type="text" name="sociedade_prof2_admissao" value="" placeholder="DATA DE ADMISSÃO">
    </fieldset>
    <fieldset>
        <div>
            <input type="text" name="sociedade_prof3" value="" placeholder="SOCIEDADE PROFISSIONAL">
        </div>
        <div>
            <label>MEMBRO ATUAL?</label>
            <input type="radio" name="sociedade_prof3_membro" value="sim" placeholder="">
            <small>SIM</small>
            <input type="radio" name="sociedade_prof3_membro" value="não" placeholder="">
            <small>NÃO</small>
        </div>
    </fieldset>
    <fieldset class="margin">
        <input type="text" name="sociedade_prof3_admissao" value="" placeholder="DATA DE ADMISSÃO">
    </fieldset>
    <fieldset>
        <button>Enviar</button>
    </fieldset>
</form>