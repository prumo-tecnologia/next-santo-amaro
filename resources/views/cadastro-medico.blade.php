@extends('layout')

@section('styles')
<link rel="stylesheet" href="/css/toastr.min.css">
@endsection

@section('content')
<section class="infoCadastro">
    <div class="wrap">
        <h1>CADASTRO MÉDICO</h1>
        <p>Quer fazer parte de nosso corpo clínico? Preencha o formulário abaixo que, assim que possível, entraremos em contato. Recomendamos ter bastante atenção e reservar um tempo para preencher todos os campos do formulário. Em caso de dúvidas, entre em contato conosco.</p>
    </div>
</section>
<section class="hospital cadastroMedico">
    <div class="wrap">
        <div class="exames cadastroMedico">
            <div class="alert">
                <ul></ul>
            </div>
            <h2>INFORMAÇÕES DO PROFISSIONAL</h2>
            @include('partials.form-cadastro-medico')
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<!-- script type="text/javascript" src="/js/additional-methods.min.js"></script -->
<script type="text/javascript" src="/js/localization/messages_pt_BR.js"></script>
<script type="text/javascript" src="/js/toastr.min.js"></script>
<script>
    $(document).ready(function() {

        toastr.options.closeButton = true;
        toastr.options.preventDuplicates = true;
        toastr.options.timeOut = 10000;
        toastr.options.extendedTimeOut = 1;

        // INFORMAÇÕES DO PROFISSIONAL
        $("input[name='nascimento']").mask('00/00/0000');
        $("input[name='crm']").mask('999999999');
        $("input[name='rg']").mask('00.000.000-0', {reverse: true});
        $("input[name='cpf']").mask('000.000.000-00', {reverse: true});
        $("input[name='residencial_cep']").mask('00000-000');
        $("input[name='consultorio_cep']").mask('00000-000');
        $("input[name='tel_residencial']").mask('(99) 9999-9999');
        $("input[name='tel_consultorio']").mask('(99) 9999-9999');
        $("input[name='tel_celular']").mask('(99) 99999-9999');
        // FORMAÇÃO PROFISSIONAL
        $("input[name='formacao_conclusao']").mask('9999');
        $("input[name='formacao_residencia_conclusao']").mask('9999');
        $("input[name='pos_instituicao_conclusao']").mask('9999');
        // REFERÊNCIAS PROFISSIONAIS
        $("input[name='ref1_crm']").mask('999999999');
        $("input[name='ref1_tel_comercial']").mask('(99) 9999-9999');
        $("input[name='ref1_tel_celular']").mask('(99) 99999-9999');
        $("input[name='ref2_crm']").mask('999999999');
        $("input[name='ref2_tel_comercial']").mask('(99) 9999-9999');
        $("input[name='ref2_tel_celular']").mask('(99) 99999-9999');
        $("input[name='ref3_crm']").mask('999999999');
        $("input[name='ref3_tel_comercial']").mask('(99) 9999-9999');
        $("input[name='ref3_tel_celular']").mask('(99) 99999-9999');
        $("input[name='ref4_crm']").mask('999999999');
        $("input[name='ref4_tel_comercial']").mask('(99) 9999-9999');
        $("input[name='ref4_tel_celular']").mask('(99) 99999-9999');
        $("input[name='ref4_crm']").mask('999999999');
        $("input[name='ref5_tel_comercial']").mask('(99) 9999-9999');
        $("input[name='ref5_tel_celular']").mask('(99) 99999-9999');
        // LISTAR A(S) SOCIEDADE(S) PROFISSIONAL QUE É AFILIADO
        $("input[name='sociedade_prof1_admissao']").mask('00/00/0000');
        $("input[name='sociedade_prof2_admissao']").mask('00/00/0000');
        $("input[name='sociedade_prof3_admissao']").mask('00/00/0000');

        var validator = $("form").validate({
            lang : 'pt_BR',
            rules: {
                // INFORMAÇÕES DO PROFISSIONAL
                nome: {
                    required: true,
                },
                crm: {
                    required: true,
                },
                nascimento: {
                    required: true,
                    validNascimento: true,
                },
                civil: {
                    required: true,
                },
                rg: {
                    required: true,
                },
                cpf: {
                    required: true,
                },
                genero: {
                    required: true,
                },
                nacionalidade: {
                    required: true,
                },
                naturalidade: {
                    required: true,
                },
                residencial: {
                    required: true,
                },
                residencial_bairro: {
                    required: true,
                },
                residencial_cep: {
                    required: true,
                },
                residencial_cidade: {
                    required: true,
                },
                residencial_estado: {
                    required: true,
                },
                consultorio: {
                    required: true,
                },
                consultorio_bairro: {
                    required: true,
                },
                consultorio_cep: {
                    required: true,
                },
                consultorio_cidade: {
                    required: true,
                },
                consultorio_estado: {
                    required: true,
                },
                tel_residencial: {
                    required: true,
                },
                tel_consultorio: {
                    required: true,
                },
                tel_celular: {
                    required: true,
                },
                email: {
                    required: true,
                },
                idioma: {
                    required: true,
                },
                outro_idioma: {
                    required: true,
                },
                especialidade: {
                    required: true,
                },
                outra_especialidade: {
                    required: true,
                },
                area_atuacao: {
                    required: true,
                },
                dados_carro: {
                    required: true,
                },
                chefe_equipe: {
                    required: true,
                },
                equipe: {
                    required: true,
                },
                // FORMAÇÃO PROFISSIONAL
                formacao_superior: {
                    required: true,
                },
                formacao_estado: {
                    required: true,
                },
                formacao_conclusao: {
                    required: true,
                },
                formacao_residencia: {
                    required: true,
                },
                formacao_residencia_estado: {
                    required: true,
                },
                formacao_residencia_conclusao: {
                    required: true,
                },
                pos_instituicao: {
                    required: true,
                },
                pos_instituicao_estado: {
                    required: true,
                },
                pos_instituicao_conclusao: {
                    required: true,
                },
                // REFERÊNCIAS PROFISSIONAIS
                ref1_nome: {
                    required: true,
                },
                ref1_crm: {
                    required: true,
                },
                ref1_instituicao: {
                    required: true,
                },
                ref1_funcao: {
                    required: true,
                },
                ref1_especialidade: {
                    required: true,
                },
                ref1_email: {
                    required: true,
                },
                ref1_tel_comercial: {
                    required: true,
                },
                ref1_tel_celular: {
                    required: true,
                },
                ref2_nome: {
                    required: true,
                },
                ref2_crm: {
                    required: true,
                },
                ref2_instituicao: {
                    required: true,
                },
                ref2_funcao: {
                    required: true,
                },
                ref2_especialidade: {
                    required: true,
                },
                ref2_email: {
                    required: true,
                },
                ref2_tel_comercial: {
                    required: true,
                },
                ref2_tel_celular: {
                    required: true,
                },
                ref3_nome: {
                    required: true,
                },
                ref3_crm: {
                    required: true,
                },
                ref3_instituicao: {
                    required: true,
                },
                ref3_funcao: {
                    required: true,
                },
                ref3_especialidade: {
                    required: true,
                },
                ref3_email: {
                    required: true,
                },
                ref3_tel_comercial: {
                    required: true,
                },
                ref3_tel_celular: {
                    required: true,
                },
                ref4_nome: {
                    required: true,
                },
                ref4_crm: {
                    required: true,
                },
                ref4_instituicao: {
                    required: true,
                },
                ref4_funcao: {
                    required: true,
                },
                ref4_especialidade: {
                    required: true,
                },
                ref4_email: {
                    required: true,
                },
                ref4_tel_comercial: {
                    required: true,
                },
                ref4_tel_celular: {
                    required: true,
                },
                ref5_nome: {
                    required: true,
                },
                ref5_crm: {
                    required: true,
                },
                ref5_instituicao: {
                    required: true,
                },
                ref5_funcao: {
                    required: true,
                },
                ref5_especialidade: {
                    required: true,
                },
                ref5_email: {
                    required: true,
                },
                ref5_tel_comercial: {
                    required: true,
                },
                ref5_tel_celular: {
                    required: true,
                },
                // LISTAR A(S) SOCIEDADE(S) PROFISSIONAL QUE É AFILIADO
                /*
                sociedade_prof1: {
                    required: true,
                },
                sociedade_prof1_membro: {
                    required: true,
                },
                sociedade_prof1_admissao: {
                    required: true,
                },
                sociedade_prof2: {
                    required: true,
                },
                sociedade_prof2_membro: {
                    required: true,
                },
                sociedade_prof2_admissao: {
                    required: true,
                },
                sociedade_prof3: {
                    required: true,
                },
                sociedade_prof3_membro: {
                    required: true,
                },
                sociedade_prof3_admissao: {
                    required: true,
                },
                */
            },
            // messages: {
            // nome: "Please enter your firstname",
            // },
            success: function(label, element) {
                toastr.clear();
                $(element).removeClass('errorcm');
            },
            errorPlacement: function (error, element) {
                // toastr.warning("Atenção ao campo: " + $(element).attr('name') + "<br />" + error.text());
                return false;
            },
            highlight: function(element, errorClass) {
                toastr.warning("Atenção ao campo: " + $(element).attr('placeholder'));
                $(element).addClass('errorcm');
            },
        });

        $.validator.addMethod("validNascimento", function(value, element) {
            return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
        }, 'Data inválida.');

        $("button").click(function() {

            $("form").submit(function( event ) {

                if(validator.valid()) {

                    $("button").addClass("loading");
                    $("button").html('ENVIANDO<span class="p1">.</span><span class="p2">.</span><span class="p3">.</span>');
                    $("button").attr('disabled', 'disabled');

                    $.ajax({
                        url: '/envio-cadastro-medico',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function (json) {
                            var info = $('.alert');
                            info.find('ul').empty();
                            if(json.success){
                                // info.find('ul').append('<li>' + json.message + '</li>');
                                toastr.success(json.message);
                            }
                            info.slideDown();
                            $("button").removeClass("loading");
                            $("button").html('ENVIAR');
                            $("button").removeAttr('disabled');
                        },
                        error: function(xhr, status, response) {
                            var error = $.parseJSON(xhr.responseText);
                            var info = $('.alert');
                            info.find('ul').empty();
                            for(var k in error.message){
                                if(error.message.hasOwnProperty(k)){
                                    error.message[k].forEach(function(val){
                                        // info.find('ul').append('<li>' + val + '</li>');
                                        toastr.error(val);
                                    });

                                }
                            }
                            info.slideDown();
                            $("button").removeClass("loading");
                            $("button").html('ENVIAR');
                            $("button").removeAttr('disabled');
                        }
                    })
                    .done(function (response) {
                        //
                    })
                    .fail(function (jqXHR, json) {
                        //
                    });
                }
                event.preventDefault();
            });
        });
    });
</script>
@endsection