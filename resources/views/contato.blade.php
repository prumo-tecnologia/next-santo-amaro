@extends('layout')

@section('content')
<div class="container bg-primary">
    <section class="banner-servicos contato wrap">
        <div class="sobreHospital contato">
            <div class="contato-col">
                <h1>FALE CONOSCO</h1>
                <p>Entre em contato conosco preenchendo os dados ao lado, que, em breve, retornaremos.</p>
            </div>
        </div>
        <div class="hospital contato">
            <div class="exames contato panel">
                @include('partials.form-contato')
            </div>
        </div>
    </section>
</div>
<div class="contato container">
    <div class="col-6">
        <p><strong>Central de Atendimento:</strong> <br>(11) 2185-0500</p><br>
        <p><strong>SAC:</strong> <br>(11) 2185-0587 <br>Email: sachmo@amil.com.br</p><br>
        <p><strong>CAF (Centro de Atendimento ao Familiar):</strong> </p>
        <p>De segunda a sexta, das 8h às 17h.</p><br>
        <p><strong>Imprensa</strong></p>
        <p>Danielle Mendonça</p>
        <p>danielle.mendonca@uhgbrasil.com.br</p>
        <p>Telefone: (11) 3375-1600</p>
        <p>Celular: (11) 94176-5909 </p><br>
        <p><strong>Central de Agendamento de Exames (Tomografia e Ecocardiograma):</strong></p>
        <p>Telefone: (11) 3003-1333</p>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        var validator = $("form").validate({
            rules: {
                nome: "required",
                assunto: {
                    maxlength: 140
                },
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                nome: "POR FAVOR PREEENCHA SEU NOME",
                assunto: "NÃO INSIRA MAIS DE 140 CARACTERES",
                email: {
                    required: "POR FAVOR ADICIONE SEU E-MAIL",
                    email: "E-MAIL: FORMATO INVALIDO",
                },
            },
            errorPlacement: function(error, element) {
                $(element).attr('placeholder', error.text());
            }
        });

        $("button").click(function() {

            $("form").submit(function(event) {

                var info = $('.alert');
                info.hide();

                if (validator.valid()) {

                    $("button").addClass("loading");
                    $("button").html('ENVIANDO<span class="p1">.</span><span class="p2">.</span><span class="p3">.</span>');
                    $("button").attr('disabled', 'disabled');

                    $.ajax({
                            url: '/envio-contato',
                            type: 'post',
                            data: $(this).serialize(),
                            dataType: "json",
                            success: function(json) {
                                info.find('ul').empty();
                                if (json.success) {
                                    info.find('ul').append('<li>' + json.message + '</li>');
                                }
                                info.slideDown();
                                $("button").removeClass("loading");
                                $("button").html('ENVIAR');
                                $("button").removeAttr('disabled');
                            },
                            error: function(xhr, status, response) {
                                var error = $.parseJSON(xhr.responseText);
                                info.find('ul').empty();
                                for (var k in error.message) {
                                    if (error.message.hasOwnProperty(k)) {
                                        error.message[k].forEach(function(val) {
                                            info.find('ul').append('<li>' + val + '</li>');
                                        });

                                    }
                                }
                                info.slideDown();
                                $("button").removeClass("loading");
                                $("button").html('ENVIAR');
                                $("button").removeAttr('disabled');
                            }
                        })
                        .done(function(response) {
                            //
                        })
                        .fail(function(jqXHR, json) {
                            //
                        });
                }
                event.preventDefault();
            });
        });
    });
</script>
@endsection