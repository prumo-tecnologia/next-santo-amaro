@extends('layout')

@section('content')
<section class="banner-servicos">
    <div class="serivcos-img-wrapper">
        <img src="images/banner-servicos.png" alt="Banner dos serviços">
    </div>
    <div class="container">
        <div id="conteudo" class="sobreHospital servicos">
            <h2>CONVÊNIOS</h2>
            <p>
                Somos credenciados para atender os principais convênios.
                Para saber sobre a cobertura do seu plano na nossa unidade hospitalar, entre em contato com a sua operadora.
                Para outras informações, fale com a nossa Central de Atendimento: (11) 2185-0500.
            </p>
            <br>
            <div class="convenio-grid">
                <ul class="col-right">
                    <li>ALLIANZ</li>
                    <li>AMIL</li>
                    <li>BRADESCO</li>
                    <li>CENTRAL NACIONAL UNIMED</li>
                    <li>CRUZ AZUL</li>
                    <li>ECONOMUS</li>
                    <li>GOLDEN CROSS</li>
                    <li>INTERCLINICAS</li>
                    <li>LIFE EMPRESARIAL</li>
                </ul>

                <ul class="col-left">
                    <li>METRUS</li>
                    <li>PETROBRÁS</li>
                    <li>PORTO SEGURO</li>
                    <li>PROASA</li>
                    <li>SANTA HELENA</li>
                    <li>SOMPO SAÚDE</li>
                    <li>SUL AMERICA</li>
                    <li>UNIMED FESP</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection