<?php $__env->startSection('content'); ?>
<section class="banner-servicos">
    <div class="serivcos-img-wrapper">
        <!-- <img src="images/banner-servicos.png" alt="Banner dos serviços"> -->
        <img src="images/maternidade-servicos.png" alt="Banner dos serviços">
    </div>
    <div class="container">

        <div id="conteudo" class="sobreHospital servicos">
            <h2>EXAMES</h2>
            <p>
                O Hospital Next Santo Amaro realiza exames de Tomografia Computadorizada, Ecocardiograma Adulto, infantil e Fetal.
                Para realiza-los, contate a Central de Agendamento pelo (11) 3003-1333.
            </p>


            <h2>CONVÊNIOS</h2>
            <p>
                Somos credenciados para atender os principais convênios.
                Para saber sobre a cobertura do seu plano na nossa unidade hospitalar, entre em contato com a sua operadora.
                Para outras informações, fale com a nossa Central de Atendimento: (11) 2185-0500.
            </p>

            <div class="convenio-grid">
                <ul class="col-right">
                    <li>ALLIANZ</li>
                    <li>AMIL</li>
                    <li>BRADESCO</li>
                    <li>CENTRAL NACIONAL UNIMED</li>
                    <li>CRUZ AZUL</li>
                    <li>ECONOMUS</li>
                    <li>GOLDEN CROSS</li>
                    <li>INTERCLINICAS</li>
                    <li>LIFE EMPRESARIAL</li>
                </ul>

                <ul class="col-left">
                    <li>METRUS</li>
                    <li>PETROBRÁS</li>
                    <li>PORTO SEGURO</li>
                    <li>PROASA</li>
                    <li>SANTA HELENA</li>
                    <li>SOMPO SAÚDE</li>
                    <li>SUL AMERICA</li>
                    <li>UNIMED FESP</li>
                </ul>
            </div>

            <h2>CENTRO DE DIAGNÓSTICO</h2>
            <p>O Centro de Diagnósticos do Hospital Next Santo Amaro fica na Rua Barão do Rio Branco, 555. O atendimento é de segunda a domingo, das 8h às 18h.</p>

        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>