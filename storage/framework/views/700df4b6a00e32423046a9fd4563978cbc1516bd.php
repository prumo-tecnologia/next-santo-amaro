<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>Hospitais Next</title>
</head>
<body>
    <table>
        <thead>
            <h1 style="font: 30px tahoma, sans-serif; color: #213f99; margin: 0 0 20px 0;"><?php echo e($fields['sb'] . ' - ' . $fields['co']); ?></h1>
        </thead>
        <tbody>
            <ul style="list-style: none;">
                <li style="padding: 20px 15px; margin: 0 0 20px 0; border-bottom: 1px solid #cccccc; width: 450px; float: left; background-color: #eeeeee;">
                    <h5 style="font: 16px tahoma, sans-serif; color: #21b8cf; margin: 0 0 5px 0; text-transform: uppercase;">Nome:</h5>
                    <p style="font: 16px tahoma, sans-serif; color: #505050; margin: 5px 0 0 0;"><?php echo e($fields['nome']); ?></p>
                </li>
                <li style="padding: 20px 15px; margin: 0 0 20px 0; border-bottom: 1px solid #cccccc; width: 450px; float: left;">
                    <h5 style="font: 16px tahoma, sans-serif; color: #21b8cf; margin: 0 0 5px 0; text-transform: uppercase;">E-mail:</h5>
                    <p style="font: 16px tahoma, sans-serif; color: #505050; margin: 5px 0 0 0;"><?php echo e($fields['email']); ?></p>
                </li>
                <li style="padding: 20px 15px; margin: 0 0 20px 0; border-bottom: 1px solid #cccccc; width: 450px; float: left; background-color: #eeeeee;">
                    <h5 style="font: 16px tahoma, sans-serif; color: #21b8cf; margin: 0 0 5px 0; text-transform: uppercase;">Assunto:</h5>
                    <p style="font: 16px tahoma, sans-serif; color: #505050; margin: 5px 0 0 0;"><?php echo e($fields['assunto']); ?></p>
                </li>
                <li style="padding: 20px 15px; margin: 0 0 20px 0; border-bottom: 1px solid #cccccc; width: 450px; float: left; background-color: #eeeeee;">
                    <h5 style="font: 16px tahoma, sans-serif; color: #21b8cf; margin: 0 0 5px 0; text-transform: uppercase;">Texto:</h5>
                    <p style="font: 16px tahoma, sans-serif; color: #505050; margin: 5px 0 0 0;"><?php echo e($fields['texto']); ?></p>
                </li>
            </ul>
        </tbody>
    </table>
</body>
</html>