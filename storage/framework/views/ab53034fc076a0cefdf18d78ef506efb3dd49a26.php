<?php $__env->startSection('content'); ?>
<section class="banner-servicos">
    <div class="container">
        <div id="conteudo" class="sobreHospital servicos">
            <h2>RELAÇÕES COM A IMPRENSA</h2>
            <p>Atendimento exclusivo a jornalistas</p>
            <p>
            <strong>Danielle Mendonça</strong><br>danielle.mendonca@uhgbrasil.com.br <br>
            Telefone: (11) 3375-1600 <br>
            Celular: (11) 94176-5909
            </p>
        </div>
    </div>
    <div class="serivcos-img-wrapper">
        <img src="images/banner-imprensa.png" alt="Banner dos serviços">
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>