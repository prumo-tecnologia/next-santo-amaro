<div class="sliderBanner sliderEstrutura">
    <div class="slide">
        <figure class="imagemBanner imagem-estrutura-1">
            <img src="images/banner-estrutura-1-desk.jpg" alt="">
        </figure>
    </div>
<!--
    <div class="slide">
        <figure class="imagemBanner imagem-estrutura-1">
            <img src="images/banner-estrutura-2-desk.jpg" alt="">
        </figure>
    </div>
-->
</div>
<div class="wrap">
    <div class="chamadaBanner estrutura">
        <span class="titulo">HORÁRIOS DE VISITA:</span>

        <p class="texto">Apartamento: Das 10H ÀS 21H</p>
        <p class="texto">Enfermaria: DAS 14H ÀS 20H</p>
        <p class="texto">UTI Adulto: Das 15:00 às 15:30 e das 20:00 às 20:30</p>
        <p class="texto"> UTI Neonatal: Sábados das 17:00 às 17:30</p>

        <span class="obs-texto">* Consulte as condições de revezamento de visitantes</span>
    </div>
</div>



