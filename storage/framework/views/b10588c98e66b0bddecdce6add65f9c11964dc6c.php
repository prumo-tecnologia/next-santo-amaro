<div class="acessibilidade">
    <div class="wrap">
        <a href="#menu" accesskey="1" title="Pressione as teclas, alt e 1 para ir ao menu do site">Ir para o menu [1]</a>
        <a href="#localizacao" accesskey="2" title="Pressione as teclas, alt e 2 para ir traçar rota">Ir para traçar rota [2]</a>
        <a href="#rodape" accesskey="3" title="Pressione as teclas, alt e 3 para ir ao rodapé do site">Ir para o rodapé [3]</a>
        <a href="javascript:void(0)" title="Clique para saber mais informações sobre a acessibilidade do site" class="linkAcessibilidade">Acessibilidade</a>
    </div>
</div>
<div class="wrap">
    <a href="<?php echo e(route('index.home')); ?>"  title="Logo do Hospitais Next">
        <img src="images/logo-santo-amaro.svg" alt="Logo do Hospitais Next" class="logo">
    </a>
    <a href="javascript:void(0)" title="Clique para abrir o menu" class="linkMenu"></a>
    <nav>
        <ul>
            <li><a name="menu" id="menu" href="<?php echo e(route('index.estrutura')); ?>" title="Veja a estrutura do Hospitais Next" class="<?php echo e((($uri == 'estrutura') ? 'active' : '')); ?>">Estrutura</a></li>
            <li><a href="<?php echo e(route('index.servicos')); ?>" title="Conheça os serviços que o Hospitais Next oferece" class="<?php echo e((($uri == 'servicos') ? 'active' : '')); ?>">Serviços</a></li>
            <li><a href="<?php echo e(route('index.imprensa')); ?>" title="Imprensa" class="<?php echo e((($uri == 'imprensa') ? 'active' : '')); ?>">Imprensa</a></li>
            <li><a href="<?php echo e(route('index.contato')); ?>" title="Entre em contato conosco" class="contato">Contato</a></li>
            <li><a href="<?php echo e(route('index.convenio')); ?>" title="convenio" class="contato">Convênios</a></li>
        </ul>
    </nav>
</div>
