 <?php $__env->startSection('content'); ?>
<section class="banner">
    <?php echo $__env->make('partials.slider-home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>
<section class="hospital no-margin destaque-home">
    <div class="wrap">
        <div id="mapa"></div>
        <div class="sobreHospital">
            <a name="localizacao" id="localizacao"></a>
            <h1>O Hospital</h1>
            <p>Aqui, você encontra as melhores práticas assistenciais e de governança clínica. Fundado em 1973, o Hospital Next Santo Amaro oferece medicina de qualidade e de custo acessível. Um dos seus principais objetivos está a busca pelo reconhecimento como Hospital Geral na prestação de serviços de saúde graças à sua constante gestão de qualidade, facilidade de localização e controle de custos.</p>
            </div>
        <div class="localizacao">
            <p class="titulo">Onde estamos</p>
            <p class="endereco">R. Barão do Rio Branco, 555 - Santo Amaro, São Paulo - SP</p>
            <a id="tracarrota" href="https://www.google.com/maps/dir/Current+Location/-23.6486968,-46.7125057" title="Traçar rota" class="tracarRota" target="_blank">Traçar rota</a>
        </div>
    </div>
</section>
<section class="especialidades">
    <div class="listaSlider">
        <h3>ESPECIALIDADES</h3>
        <p class="texto">Confira as especialidades atendidas em nosso Centro Médico:</p>
        <div class="slider">
            <div>
                <img src="images/icons/tooth.svg" alt="Icone Angiologia cirurgia vascular e linfática">
                <p>Cirurgia Bucomaxilofacial</p>
            </div>
            <div>
                <img src="images/icons/heart.svg" alt="Icone Arritmologia">
                <p>Cardiologia</p>
            </div>
            <div>
                <img src="images/icons/operating-room.svg" alt="Icone Cardiologia">
                <p>Cirurgia Tórax</p>
            </div>
            <div>
                <img src="images/icons/heart.svg" alt="Icone Cardiologia intervencionista">
                <p>Cirurgia Vascular</p>
            </div>
            <div>
                <img src="images/icons/heart.svg" alt="Icone Cirurgia cardíaca">
                <p>Cirurgia Ginecológica</p>
            </div>
            <div>
                <img src="/images/icons/heart.svg" alt="Icone Cirurgia cardíaca hemodinâmica">
                <p>Nefrologia</p>
            </div>
            <div>
                <img src="images/icons/neck.svg" alt="Icone Cirurgia de cabeça e pescoço">
                <p>Neurologia</p>
            </div>
            <div>
                <img src="images/icons/operating-room.svg" alt="Icone Cirurgia geral">
                <p>Ortopedia e Traumatologia</p>
            </div>
            <div>
                <img src="images/icons/plastic-surgery.svg" alt="Icone Cirurgia plástica">
                <p>Pneumologia</p>
            </div>
            <div>
                <img src="images/icons/phonendoscope.svg" alt="Icone Clínica médica">
                <p>Urologia</p>
            </div>
            <div>
                <img src="images/icons/intestine.svg" alt="Icone Coloproctologia">
                <p>Cirurgia Geral</p>
            </div>
            <div>
                <img src="images/icons/pimples.svg" alt="Icone Dermatologia clínico-cirúrgica">
                <p>Pediatria</p>
            </div>
            <div>
                <img src="images/icons/hexagons.svg" alt="Icone Endocrinologia">
                <p>Cirurgia Pediátrica</p>
            </div>
            <div>
                <img src="images/icons/stomach.svg" alt="Icone Gastroenterologia">
                <p>Clínica Médica</p>
            </div>
            <div>
                <img src="images/icons/couple.svg" alt="Icone Geriatria e Gerontologia">
                <p>Infectologia</p>
            </div>
            <div>
                <img src="images/icons/alergy.svg" alt="Icone Ginecologia">
                <p>Hematologia</p>
            </div>
            <div>
                <img src="images/icons/alergy.svg" alt="Icone Terapia Intensiva">
                <p>Psiquiatria</p>
            </div>
            <div>
                <!-- NEW -->
                <img src="images/icons/alergy.svg" alt="Icone Terapia Intensiva">
                <p>Ginecologia e Obstetrícia</p>
            </div>
            <div>
                <!-- NEW -->
                <img src="images/icons/alergy.svg" alt="Icone Terapia Intensiva">
                <p>Endoscopia</p>
            </div>
            <div>
                <!-- NEW -->
                <img src="images/icons/alergy.svg" alt="Icone Terapia Intensiva">
                <p>Colonoscopia</p>
            </div>
            <div>
                <!-- NEW -->
                <img src="images/icons/alergy.svg" alt="Icone Terapia Intensiva">
                <p>CPRE</p>
            </div>
            <div>
                <!-- NEW -->
                <img src="images/icons/alergy.svg" alt="Icone Terapia Intensiva">
                <p>Neomatologia</p>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="lista">
            <h3>ESPECIALIDADES MÉDICAS</h3>
            <p>Clinica Médica / Cirurgia Geral / Urologia / Bucomaxilo / Ginecologia e Obstetricia / Neonatologia /
                Ortopedia e Traumatologia / Pediatria / Cirurgia Pediátrica</p>
        </div>
        <?php /*<div class="video">
            <img src="images/especialidades-medicas.jpg" alt="Foto equipe hospital Carlos Chagas">
        </div>*/ ?>
    </div>

    <div class="selo-ona">
        <img src="images/path-ona.png">
    </div>

</section>
<?php $__env->stopSection(); ?> <?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="js/jquery.mobile.min.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDW0z3FM2m7_Gm-Dl4uS2-tlficwjIqkiw"></script>
<script type="text/javascript" src="js/gmaps.js"></script>
<script>
    $(document).ready(function() {
        $('.sliderBanner, .listaSlider .slider').slick({
            accessibility: true,
            autoplay: true,
            slidesToShow: 1,
            slidesToScroll: 1
        });
        $('.sliderBanner').addClass('opacity');
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>