<div class="sliderBanner">

    <!-- banner exames de imagem -->
    <div class="slide">
        <div class="wrap">
            <div class="chamadaBanner">

                <span class="titulo">Exames de imagem</span>
                <a href="http://portalresultados.amil.com.br/BR_Customizations/ResultadosExames/" target="_blank" title="Exames de Imagem">ACESSE AQUI &#62;&#62;</a>
            </div>
        </div>
        <div class="mask"></div>
        <div class="imagemBanner imagem-home-6"></div>
    </div>

    <!-- banner resultado do exame de covid 19 -->
    <div class="slide">
        <div class="wrap">
            <div class="chamadaBanner">

                <span class="titulo">Resultado Covid 19 RT-PCR</span>
                <a href="https://examescovid.uhgbrasil.com.br/portal/login" title="Portal de Reultados de Exames">ACESSE AQUI &#62;&#62;</a>

                </a>
            </div>
        </div>
        <div class="mask"></div>
        <div class="imagemBanner imagem-home-4"></div>
    </div>

    <div class="slide">
        <div class="wrap">
            <div class="chamadaBanner">

                <span class="titulo">Quando se fala em saúde, é preciso oferecer sempre o melhor.
                </span>
                <a href="/santo-amaro/estrutura" title="Estrutura moderna e completa para seu atendimento">Conheça &#62;&#62;
                </a>
            </div>
        </div>
        <div class="mask"></div>
        <div class="imagemBanner imagem-home-1"></div>
    </div>
</div>