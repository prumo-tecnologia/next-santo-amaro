

<?php $__env->startSection('content'); ?>
<section class="banner-servicos">
    <div class="serivcos-img-wrapper">
        <img src="images/banner-servicos.png" alt="Banner dos serviços">
    </div>
    <div class="container">
        <div id="conteudo" class="sobreHospital servicos">
            <h2>EXAMES</h2>
            <p>O Hospital Next Santo Amaro realiza exames de Tomografia Computadorizada, Ecocardiograma Adulto, infantil e Fetal.
Para realiza-los, contate a Central de Agendamento pelo (11) 3003-1333.</p>
            <br>
            <h2>CENTRO DE DIAGNÓSTICO</h2>
            <p>O Centro de Diagnósticos do Hospital Next Santo Amaro fica na Rua Barão do Rio Branco, 555. O atendimento é de segunda a domingo, das 8h às 18h.</p>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>