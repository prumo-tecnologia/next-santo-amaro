

<?php $__env->startSection('content'); ?>
<div class="container bg-primary">
    <section class="banner-servicos contato wrap">
        <div class="sobreHospital contato">
            <div class="contato-col">
                <h1>FALE CONOSCO</h1>
                <p>Entre em contato conosco preenchendo os dados ao lado, que, em breve, retornaremos.</p>
            </div>
        </div>
        <div class="hospital contato">
            <div class="exames contato panel">
                <?php echo $__env->make('partials.form-contato', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </section>
</div>
<div class="contato container">
    <div class="col-6">
        <p><strong>Central de Atendimento:</strong> <br>(11) 2185-0500</p>
        <p><strong>SAC:</strong> <br>(11) 2185-0587 <br>Email: sachmo@amil.com.br</p>
        <p><strong>CAF (Centro de Atendimento ao Familiar):</strong> <br>De segunda a sexta, das 8h às 17h.</p>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        var validator = $("form").validate({
            rules: {
                nome: "required",
                assunto: {
                    maxlength: 140
                },
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                nome: "POR FAVOR PREEENCHA SEU NOME",
                assunto: "NÃO INSIRA MAIS DE 140 CARACTERES",
                email: {
                    required: "POR FAVOR ADICIONE SEU E-MAIL",
                    email: "E-MAIL: FORMATO INVALIDO",
                },
            },
            errorPlacement: function (error, element) {
                $(element).attr('placeholder', error.text());
            }
        });

        $("button").click(function() {

            $("form").submit(function( event ) {

                var info = $('.alert');
                info.hide();

                if(validator.valid()) {

                    $("button").addClass("loading");
                    $("button").html('ENVIANDO<span class="p1">.</span><span class="p2">.</span><span class="p3">.</span>');
                    $("button").attr('disabled', 'disabled');

                    $.ajax({
                        url: '/envio-contato',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function (json) {
                            info.find('ul').empty();
                            if(json.success){
                                info.find('ul').append('<li>' + json.message + '</li>');
                            }
                            info.slideDown();
                            $("button").removeClass("loading");
                            $("button").html('ENVIAR');
                            $("button").removeAttr('disabled');
                        },
                        error: function(xhr, status, response) {
                            var error = $.parseJSON(xhr.responseText);
                            info.find('ul').empty();
                            for(var k in error.message){
                                if(error.message.hasOwnProperty(k)){
                                    error.message[k].forEach(function(val){
                                        info.find('ul').append('<li>' + val + '</li>');
                                    });

                                }
                            }
                            info.slideDown();
                            $("button").removeClass("loading");
                            $("button").html('ENVIAR');
                            $("button").removeAttr('disabled');
                        }
                    })
                    .done(function (response) {
                        //
                    })
                    .fail(function (jqXHR, json) {
                        //
                    });
                }
                event.preventDefault();
            });
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>