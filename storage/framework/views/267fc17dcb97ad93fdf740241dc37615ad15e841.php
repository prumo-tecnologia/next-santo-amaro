<div class="wrap">
    <div class="contato">
        <div>
            <p class="titulo">Central de Atendimento:</p>
            <p class="info">(11) 2185-0500</p>
        </div>
        <div>
            <p class="titulo">SAC:</p>
            <p class="info marginBottom">(11) 2185-0587</p>
        </div>
        <div>
            <p class="titulo">E-mail SAC:</p>
            <p class="info marginBottom"><a href="mailto:sachmo@amil.com.br">sachmo@amil.com.br</a></p>
        </div>
    </div>
    <nav>
        <ul>
            <li><a name="rodape" id="rodape" href="<?php echo e(route('index.estrutura')); ?>" title="ESTRUTURA" class="<?php echo e((($uri == 'estrutura') ? 'active' : '')); ?>">ESTRUTURA</a></li>
            <li><a href="<?php echo e(route('index.servicos')); ?>" title="SERVIÇOS" class="<?php echo e((($uri == 'servicos') ? 'active' : '')); ?>">SERVIÇOS</a></li>
            <li><a href="<?php echo e(route('index.imprensa')); ?>" title="IMPRENSA" class="<?php echo e((($uri == 'imprensa') ? 'active' : '')); ?>">IMPRENSA</a></li>
            <!--<li><a href="<?php echo e(route('index.convenio')); ?>" title="CONVÊNIO" class="<?php echo e((($uri == 'convenio') ? 'active' : '')); ?>">Convênios</a></li>-->
            <li><a href="<?php echo e(route('index.contato')); ?>" title="CONTATO" class="<?php echo e((($uri == 'contato') ? 'active' : '')); ?>">CONTATO</a></li>
        </ul>
    </nav>
</div>
<div class="assinatura">
    <p>Responsável Técnico: Dra. Ana Carolina Martins Costa Juliano – CRM: 126.483</p>
    <p><a href="/pdf/CodeOfConduct.pdf" target="_blank" style="text-decoration: underline;"> C&oacute;digo de conduta para parceiro de neg&oacute;cios</a> | <a href="/pdf/CodigoDeCondutaUHG2017.pdf" title="Código de Conduta UHG" target="_blank" style="text-decoration: underline;">Código de Conduta UHG</a> | <a href="http://hospitaisnext.com.br/comunicado/" target="_blank" style="text-decoration: underline;">Comunicado de privacidade</a> - Hospitais Next - Marca Registrada. Todos os direitos reservados. <a href="http://www.casademarcas.com.br/" title="Casa de Marcas" target="_blank">Desenvolvido por Casa de Marcas</a></p>
</div>