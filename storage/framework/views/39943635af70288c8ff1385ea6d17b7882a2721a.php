<?php $__env->startSection('content'); ?>
<section class="banner mar_b-md no-mar_b-md">
  <?php echo $__env->make('partials.slider-estrutura', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>

<!-- PopUp -->
<div class="popup-show">
  <div id="popup-1" class="popup">
    <div class="overlay"></div>
    <div class="content">
      <div class="close-btn" onclick="popupClose();">&times;</div>
      <h1>IMPORTANTE</h1>
      <p>As visitas estão temporariamente suspensas devido ao COVID-19</p>
    </div>
  </div>
</div>

<section class="hospital estrutura no-mar_b-md">
  <div class="sliderEstrutura">
    <div class="slide">
      <div class="mask"></div>
      <img src="images/slider-estrutura-1.jpg" alt="Foto da estrutura do hospital Carlos Chagas">
    </div>
    <div class="slide">
      <div class="mask"></div>
      <img src="images/slider-estrutura-2.jpg" alt="Foto da estrutura do hospital Carlos Chagas">
    </div>
    <!--
        <div class="slide">
            <div class="mask"></div>
            <img src="images/slider-estrutura-3.jpg" alt="Foto da estrutura do hospital Carlos Chagas">
        </div>
  -->
  </div>
  <div class="wrap">
    <div class="sobreHospital estrutura">
      <h1>ESTRUTURA</h1>
      <p>Com uma estrutura pronta para receber casos de diferentes níveis de complexidade, o Hospital Next Santo Amaro conta com:</p>
      <ul>
        <li>
          <p>- Leitos de UTI (adulto)</p>
        </li>
        <li>
          <p>- Leitos de Internação e de observação</p>
        </li>
        <li>
          <p>- Pronto Socorro 24h de Clinica Médica, Ortopedia, Bucomaxilo e Pediatria</p>
        </li>
      </ul>
      <p>Na Unidade, os pacientes são acolhidos por uma equipe multidisciplinar altamente capacitada, visando a resolução do diagnósticos de forma ágil, eficaz e acolhedora.</p>

      <?php /*<p>O hospital conta com atendimento para internações eletivas em tempo integral (24h). O agendamento pode ser feito através da Central de Agendamento ou por meio da secretária do seu médico/consultório.</p>
            <p><strong>Dúvidas no telefone  (11) 4122-8300</strong></p>*/ ?>
    </div>
    <div class="chamadaEstrutura shapeBanner">
      <span class="titulo">HORÁRIOS DE VISITA:</span>
      <span class="texto">Enfermaria: Diariamente das 14:00 às 20:00 <br>Permitido 1 visitante por leito em esquema de revezamento dentro do horário.</span>
      <span class="texto">Apartamento: Diariamente das 10:00 às 21:00<br>Permitido 1 acompanhante 24 horas, mais 02 visitantes em esquema de revezamento dentro do horário.</span>
      <span class="texto">UTI Adulto: Diariamente das 15:00 às 15:30 e das 20:00 às 20:30<br>Permitido 2 visitantes por leito, sem revezamento.</span>
    </div>
  </div>
</section>

<section class="banner-servicos no-pad utiAbout">
  <div class="container">
    <div class="sobreHospital servicos bg-alternative">
      <div class="sobreUti pad-sm">
        <h2>HORÁRIOS DE VISITA</h2>
        <p class="texto">Enfermaria: Diariamente das 14:00 às 20:00<br>Permitido 1 visitante por leito em esquema de revezamento dentro do horário.</p>
        <p class="texto">Apartamento: Diariamente das 10:00 às 21:00<br> Permitido 1 acompanhante 24 horas, mais 02 visitantes em esquema de revezamento dentro do horário.</p>
        <p class="info">UTI Adulto: Diariamente das 15:00 às 15:30 e das 20:00 às 20:30<br>Permitido 2 visitantes por leito, sem revezamento.</p>
        <br>
      </div>
    </div>
  </div>
  <figure class="serivcos-img-wrapper">
    <img src="images/uti.jpg" alt="Imagem da UTI do Hospitais Next">
  </figure>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="js/slick.min.js"></script>
<script>
  $(document).ready(function() {
    $('.listaSlider .slider').slick({
      accessibility: true,
      autoplay: true,
      slidesToShow: 1,
      slidesToScroll: 1
    });
    $('.sliderEstrutura').slick({
      accessibility: true,
      autoplay: true,
      slidesToShow: 1,
      slidesToScroll: 1
    });
    $('.sliderBanner').addClass('opacity');
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>