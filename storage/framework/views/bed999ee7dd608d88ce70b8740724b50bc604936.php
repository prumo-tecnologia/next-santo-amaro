<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5QLS9K5');</script><!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="csrf-token" content="0526y18aK13uJcDrTrq0UE9NBiH1xC3ZqtgFTgOa<?php // {{ csrf_token() }} ?>">
    <link rel="shortcut icon" href="favicon.png">
    <title>Hospitais Next</title>
    <link rel="stylesheet" href="css/style.css?01">
    <link rel="stylesheet" href="css/responsive.css?01">
    <?php echo $__env->yieldContent('styles'); ?>
    
</head>
<body>
<header>
    <!-- Google Tag Manager (noscript) --> <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QLS9K5" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <!-- End Google Tag Manager (noscript) -->
   
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110409786-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-110409786-1');
    </script>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</header>

<?php echo $__env->yieldContent('content'); ?>

<a href="tel:2139788000" title="Clique para ligar para o Hospitais Next" class="callMobile"></a>

<section class="convenios">
    <div class="modalConvenios">
        <div class="modal">
            <p class="titulo">Acessibilidade</p>
            <p class="texto">
                Nós, do Hospitais Next, preocupados com a necessidade de levar nossos serviços para o maior número de pessoas possível, inauguramos o nosso novo site de um jeito diferente.
                <br><br>
                Trazemos para a população um site com recursos de acessibilidade, seguindo recomendações do W3C (World Web Consortium), principal organização de padronização da web, com orientações específicas de acessibilidade para as empresas se tornarem cada vez mais acessíveis para pessoas com deficiência.
                <br><br>
                Com isso, pessoas com deficiência visual e mobilidade reduzida podem navegar em nosso site por meio de recursos que foram implementados para garantir este acesso e realizar a leitura dos textos por leitores de sites, teclas de atalho e navegação por teclado.
                <br><br>
                Para pessoas com baixa visão, é possível aumentar a fonte das letras, quando for necessário, utilizando o zoom nativo do navegador, pressionando as teclas “Ctrl” e “+” para aumentar todo o site e “Ctrl” e “-“ para diminuir. Para voltar ao padrão, pressione “Ctrl” e “0”.
                <br><br>
                Este site tem melhor acessibilidade quando acessado nas versões mais atualizadas do seu navegador web. Utilize sempre a versão mais recente de seu software.
                <br><br>
                Teclas de atalho por navegadores:
                <br><br>
                Internet Explorer:<br>
                <span><img src="images/icone-windows.svg" alt=""></span>[Alt] + accesskey<br>
                <span><img src="images/icone-linux.svg" alt=""></span>[Alt] + accesskey
                <br><br>
                Chrome:<br>
                <span><img src="images/icone-windows.svg" alt=""></span>[Alt] + accesskey<br>
                <span><img src="images/icone-linux.svg" alt=""></span>[Alt] + accesskey<br>
                <span><img src="images/icone-apple.svg" alt=""></span>[Control] [Alt] + accesskey
                <br><br>
                Firefox:<br>
                <span><img src="images/icone-windows.svg" alt=""></span>[Alt] [Shift] + accesskey<br>
                <span><img src="images/icone-linux.svg" alt=""></span>[Alt] [Shift] + accesskey<br>
                <span><img src="images/icone-apple.svg" alt=""></span>[Control] [Alt] + accesskey
                <br><br>
                Safari:<br>
                <span><img src="images/icone-windows.svg" alt=""></span>[Alt] + accesskey<br>
                <span><img src="images/icone-apple.svg" alt=""></span>[Control] [Alt] + accesskey
                <br><br>
                Opera:<br>
                <span><img src="images/icone-windows.svg" alt=""><img src="images/icone-linux.svg" alt=""><img src="images/icone-apple.svg" alt=""></span><br>
                Opera 15 or newer: [Alt] + accesskey | Opera 12.1 or older: [Shift] [Esc] + accesskey
                <br><br>
                <b>Navegação por tabulação</b>
                <br><br>
                Use a tecla Tab para navegar por elementos que recebem ação do usuário no site, tais como links, botões, campos de formulário e outros, na ordem em que eles são apresentados na página. Para retornar, utilize Shift + Tab. Use as setas direcionais para acessar as informações textuais.
                <br><br>
                Sugestões de programas disponíveis para pessoas com deficiência:
                <br><br>
                - Nitrous Voice Flux: controla o computador por voz. (Gratuito);<br>
                - NVDA: software livre para leitura de tela – vários idiomas (Windows);<br>
                - YeoSoft Text: leitor de tela em inglês e português;<br>
                - Jaws for Windows: leitor de tela – vários idiomas;<br>
                - Virtual Vision: leitor de tela em português do Brasil;<br>
                - DOSVOX: sistema para deficientes visuais (Windows ou Linux).<br>
                <br><br>
                Observação: leia no manual do leitor de telas sobre a melhor forma de navegação em páginas web.
            </p>
        </div>
    </div>
</section>

<footer class="top">
    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</footer>

<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<?php echo $__env->yieldContent('scripts'); ?>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
