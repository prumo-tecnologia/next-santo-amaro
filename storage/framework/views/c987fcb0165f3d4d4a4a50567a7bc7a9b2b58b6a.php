<div class="sliderBanner sliderEstrutura">
    <div class="slide">
        <figure class="imagemBanner imagem-estrutura-1">
            <img src="images/banner-estrutura-1-desk.jpg" alt="">
        </figure>
    </div>
<!--
    <div class="slide">
        <figure class="imagemBanner imagem-estrutura-1">
            <img src="images/banner-estrutura-2-desk.jpg" alt="">
        </figure>
    </div>
-->
</div>
<div class="wrap">
    <div class="chamadaBanner estrutura">
        <span class="titulo">HORÁRIOS DE VISITA:</span>
        <span class="texto">Apartamento: Das 10H ÀS 21H <br>Enfermaria: DAS 14H ÀS 20H<br>UTI Adulto: Das 15:00 às 15:30 e das 20:00 às 20:30<br> UTI Neonatal: Sábados das 17:00 às 17:30</span>
        <span class="obs-texto">* Consulte as condições de revezamento de visitantes</span>
    </div>
</div>



