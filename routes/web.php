<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
$app->get('/', function () use ($app) {
    return $app->version();
});
*/

$app->get('/', ['as' => 'index.home', 'uses' => 'IndexController@index']);

$app->get('/estrutura', ['as' => 'index.estrutura', 'uses' => 'IndexController@estrutura']);

$app->get('/servicos', ['as' => 'index.servicos', 'uses' => 'IndexController@servicos']);

$app->post('/envio-cadastro-medico', ['as' => 'index.envio-cadastro-medico', 'uses' => 'IndexController@enviocadastromedico']);

$app->get('/imprensa', ['as' => 'index.imprensa', 'uses' => 'IndexController@imprensa']);

$app->get('/contato', ['as' => 'index.contato', 'uses' => 'IndexController@contato']);

$app->get('/convenio', ['as' => 'index.convenio', 'uses' => 'IndexController@convenio']);

$app->post('/envio-contato', ['as' => 'index.envio-contato', 'uses' => 'IndexController@enviocontato']);