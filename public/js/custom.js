$(document).ready(function() {

	$('.linkAcessibilidade').click(function(event) {
		$('.modalConvenios').removeClass('hideModal');
		$('.modalConvenios').addClass('showModal');
		$('body').addClass('noScroll');
		setTimeout(function(){
			$('.modal, .verSiteModal').addClass('openModal');
		}, 1000);
		setTimeout(function(){
			$('.modalConvenios').addClass('cursorFechar');
		}, 1200);
	});

	$('.linkMenu').click(function(event) {
		$('header nav').toggleClass('openMenu');
		$(this).toggleClass('fecharMenu');
		setTimeout(function(){
			$('header nav ul li').toggleClass('showLinksMenu');
		}, 600);
	});

	$(window).scroll(function() {
		var topOfWindow = $(window).scrollTop();
		if (topOfWindow > 50) {
			$('header .acessibilidade').addClass("hideAcessibilidade");
		} else {
			$('header .acessibilidade').removeClass("hideAcessibilidade");
		}
	});

	// FUNÇÃO PARA FECHAR O MENU COM EVENTO DE SWIPE QUANDO FOR UM DISPOSITIVO MOBILE
	var supportTouch = $.support.touch,
	            scrollEvent = "touchmove scroll",
	            touchStartEvent = supportTouch ? "touchstart" : "mousedown",
	            touchStopEvent = supportTouch ? "touchend" : "mouseup",
	            touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
	    $.event.special.swipeupdown = {
	        setup: function() {
	            var thisObject = this;
	            var $this = $(thisObject);
	            $this.bind(touchStartEvent, function(event) {
	                var data = event.originalEvent.touches ?
	                        event.originalEvent.touches[ 0 ] :
	                        event,
	                        start = {
	                            time: (new Date).getTime(),
	                            coords: [ data.pageX, data.pageY ],
	                            origin: $(event.target)
	                        },
	                        stop;

	                function moveHandler(event) {
	                    if (!start) {
	                        return;
	                    }
	                    var data = event.originalEvent.touches ?
	                            event.originalEvent.touches[ 0 ] :
	                            event;
	                    stop = {
	                        time: (new Date).getTime(),
	                        coords: [ data.pageX, data.pageY ]
	                    };

	                    // prevent scrolling
	                    if (Math.abs(start.coords[1] - stop.coords[1]) > 10) {
	                        event.preventDefault();
	                    }
	                }
	                $this
	                        .bind(touchMoveEvent, moveHandler)
	                        .one(touchStopEvent, function(event) {
	                    $this.unbind(touchMoveEvent, moveHandler);
	                    if (start && stop) {
	                        if (stop.time - start.time < 1000 &&
	                                Math.abs(start.coords[1] - stop.coords[1]) > 30 &&
	                                Math.abs(start.coords[0] - stop.coords[0]) < 75) {
	                            start.origin
	                                    .trigger("swipeupdown")
	                                    .trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
	                        }
	                    }
	                    start = stop = undefined;
	                });
	            });
	        }
	    };
	    $.each({
	        swipedown: "swipeupdown",
	        swipeup: "swipeupdown"
	    }, function(event, sourceEvent){
	        $.event.special[event] = {
	            setup: function(){
	                $(this).bind(sourceEvent, $.noop);
	            }
	        };
	    });

	// FUNÇÃO QUE DEFINE QUAL O MÉTODO DE FECHAR O MENU PARA CADA DISPOSTIVO (MOBILE OU DESKTOP)

	function fecharMenu(){
		if( $(window).width() > 1024 ){
			$('.modal, .modalConvenios').click(function(event) {
				$('body, html').removeClass('noScroll');
				$('.modal, .verSiteModal').removeClass('openModal');
				setTimeout(function(){
					$('.modalConvenios').addClass('hideModal');
					$('.modalConvenios').removeClass('cursorFechar');
				}, 800);
				setTimeout(function(){
					$('.modalConvenios').removeClass('showModal');
					$('.modalConvenios').hide();
				}, 1200);
			});
		} else {
			$('.modalConvenios, .modal, .modal p, .modal a').on('swipedown',function(){
				$('body, html').removeClass('noScroll');
				$('.modal, .verSiteModal').removeClass('openModal');
				setTimeout(function(){
					$('.modalConvenios').addClass('hideModal');
					$('.modalConvenios').removeClass('cursorFechar');
				}, 800);
				setTimeout(function(){
					$('.modalConvenios').removeClass('showModal');
					$('.modalConvenios').hide();
				}, 1200);
			});
			$('.modalConvenios, .modal, .modal p, .modal a').on('swipeup',function(){
				$('body, html').removeClass('noScroll');
				$('.modal, .verSiteModal').removeClass('openModal');
				setTimeout(function(){
					$('.modalConvenios').addClass('hideModal');
					$('.modalConvenios').removeClass('cursorFechar');
				}, 800);
				setTimeout(function(){
					$('.modalConvenios').removeClass('showModal');
					$('.modalConvenios').hide();
				}, 1200);
			});
			$('.modalConvenios .modal, .modalConvenios .modal .titulo').click(function() {
				$('body, html').removeClass('noScroll');
				$('.modal, .verSiteModal').removeClass('openModal');
				setTimeout(function(){
					$('.modalConvenios').addClass('hideModal');
					$('.modalConvenios').removeClass('cursorFechar');
				}, 800);
				setTimeout(function(){
					$('.modalConvenios').removeClass('showModal');
					$('.modalConvenios').hide();
				}, 1200);
			});
		}
	}

	fecharMenu();

	$(window).resize(function() {
		fecharMenu();
	});

    $('a[accesskey="1"]').click(function(e) {
        e.preventDefault();
        $('#menu').focus();
    });

    $('a[accesskey="2"]').click(function(e) {
        e.preventDefault();

        if ($('#localizacao').length > 0) {
            $('#tracarrota').focus();
        } else {
            window.location.href = "/#localizacao";
        }
    });

    $('a[accesskey="3"]').click(function(e) {
        e.preventDefault();
        if ($('#convenios').length > 0) {
            $('#planos').focus();
        } else {
            window.location.href = "/#convenios";
        }
    });

    $('a[accesskey="4"]').click(function(e) {
        e.preventDefault();
        $('#rodape').focus();
    });

    // var hash = window.location.hash.substr(1);
    var hash = window.location.hash;

    if ((hash == "#localizacao") && ($(hash).length > 0)) {
        $('#tracarrota').focus();
    } else if ((hash == "#convenios") && ($(hash).length > 0)) {
        $('#planos').focus();
    }
});

function optionsAppendSelect() {
	if (!$(window).width() > 992 || $('#listEspec').length <= 0) return;

	var offserFromParent = $('.banner-servicos .container .sobreHospital.servicos form').position().top;
	var heightInnerDiv = $('.banner-servicos .container .sobreHospital.servicos').innerHeight();
	var heightSelect = $('#listEspec').innerHeight();

	var transformValue = (heightInnerDiv - offserFromParent) - (heightSelect / 2);

	$('.exames.corpoClinico.child-select').css('transform', 'translateY(-' + Math.floor(transformValue) + 'px)');
}

function equalHeightSlide() {
	var estruturaHeight = $('.sobreHospital.estrutura').innerHeight();
	console.log(estruturaHeight);
	$('.hospital.estrutura .sliderEstrutura').css('height', estruturaHeight + 'px');
}

var events = ['resize', 'load'];

events.forEach(function (el) {
	$(window).on(el, function() {
		optionsAppendSelect();
		equalHeightSlide();
	});
});



function popupClose(){
	document.getElementById('popup-1').classList.add('popup-remove');
	console.log('remove');
}