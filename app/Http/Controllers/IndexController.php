<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     *
     */
    public function __construct(Request $request)
    {
        $uri = $request->path();

        View::share('uri', $uri);
    }

    public function index()
    {
        return view('home');
    }

    public function estrutura()
    {
        return view('estrutura');
    }

    public function convenio()
    {
        return view('convenio');
    }

    public function servicos()
    {
        return view('servicos');
    }

    public function corpoclinico()
    {
        $arr = IndexController::csv_to_array(storage_path() . '/CorpoClinicoHNext.csv', $delimiter=',');

        $corpocli = array();
        $corposec = array();

        foreach ($arr as $corpo) {

            if(($nodes = explode('/', $corpo['Especialidade'])) && (count($nodes) > 1)){

                $tee = "";

                foreach ($nodes as $kn => $vn) {

                    $tsl = IndexController::slugify($vn);

                    $corposec[$tsl] = trim($vn);

                    $tee = $tee . $tsl;

                    if ($kn != count($nodes) - 1) {
                        $tee = $tee . "|";
                    }
                }

                $corpo['Slug'] = $tee;

            } else {
                $esl = IndexController::slugify($corpo['Especialidade']);
                $corposec[$esl] = trim($corpo['Especialidade']);
                $corpo['Slug'] = $esl;
            }

            $corpocli[] = $corpo;
        }

        // MZ: nao entendi porque o array apresenta essa entrada?
        $corposec = array_except($corposec, 'n-a');

        ksort($corposec);

        return view('corpo-clinico', compact('corpocli', 'corposec'));
    }

    /* MZ: o site do Hospitais Next nao tem area medica;
    public function areamedica()
    {
        return view('area-medica');
    }
    */

    /* MZ: o site do Hospitais Next nao tem cadastro medico;
    public function cadastromedico ()
    {
        return view('cadastro-medico');
    }

    public function enviocadastromedico (Request $request)
    {
        $rdata = $request->all();

        $validator = Validator::make($rdata, array(
            // INFORMAÇÕES DO PROFISSIONAL
            'nome' => 'required|min:3|max:180',
            'crm' => 'required',
            'nascimento' => 'required',
            'civil' => 'required',
            'rg' => 'required',
            'cpf' => 'required',
            'genero' => 'required',
            'nacionalidade' => 'required',
            'naturalidade' => 'required',
            'residencial' => 'required',
            'residencial_bairro' => 'required',
            'residencial_cep' => 'required',
            'residencial_cidade' => 'required',
            'residencial_estado' => 'required',
            'consultorio' => 'required',
            'consultorio_bairro' => 'required',
            'consultorio_cep' => 'required',
            'consultorio_cidade' => 'required',
            'consultorio_estado' => 'required',
            'tel_residencial' => 'required',
            'tel_consultorio' => 'required',
            'tel_celular' => 'required',
            'email' => 'required',
            'idioma' => 'required',
            'outro_idioma' => 'required',
            'especialidade' => 'required',
            'outra_especialidade' => 'required',
            'area_atuacao' => 'required',
            'dados_carro' => 'required',
            'chefe_equipe' => 'required',
            'equipe' => 'required',
            // FORMAÇÃO PROFISSIONAL
            'formacao_superior' => 'required',
            'formacao_estado' => 'required',
            'formacao_conclusao' => 'required',
            'formacao_residencia' => 'required',
            'formacao_residencia_estado' => 'required',
            'formacao_residencia_conclusao' => 'required',
            'pos_instituicao' => 'required',
            'pos_instituicao_estado' => 'required',
            'pos_instituicao_conclusao' => 'required',
            // REFERÊNCIAS PROFISSIONAIS
            'ref1_nome' => 'required',
            'ref1_crm' => 'required',
            'ref1_instituicao' => 'required',
            'ref1_funcao' => 'required',
            'ref1_especialidade' => 'required',
            'ref1_email' => 'required',
            'ref1_tel_comercial' => 'required',
            'ref1_tel_celular' => 'required',
            'ref2_nome' => 'required',
            'ref2_crm' => 'required',
            'ref2_instituicao' => 'required',
            'ref2_funcao' => 'required',
            'ref2_especialidade' => 'required',
            'ref2_email' => 'required',
            'ref2_tel_comercial' => 'required',
            'ref2_tel_celular' => 'required',
            'ref3_nome' => 'required',
            'ref3_crm' => 'required',
            'ref3_instituicao' => 'required',
            'ref3_funcao' => 'required',
            'ref3_especialidade' => 'required',
            'ref3_email' => 'required',
            'ref3_tel_comercial' => 'required',
            'ref3_tel_celular' => 'required',
            'ref4_nome' => 'required',
            'ref4_crm' => 'required',
            'ref4_instituicao' => 'required',
            'ref4_funcao' => 'required',
            'ref4_especialidade' => 'required',
            'ref4_email' => 'required',
            'ref4_tel_comercial' => 'required',
            'ref4_tel_celular' => 'required',
            'ref5_nome' => 'required',
            'ref5_crm' => 'required',
            'ref5_instituicao' => 'required',
            'ref5_funcao' => 'required',
            'ref5_especialidade' => 'required',
            'ref5_email' => 'required',
            'ref5_tel_comercial' => 'required',
            'ref5_tel_celular' => 'required',
            // LISTAR A(S) SOCIEDADE(S) PROFISSIONAL QUE É AFILIADO
        ));

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 422);
        }

        $rdts = array(
            // INFORMAÇÕES DO PROFISSIONAL
            'nome' => $rdata['nome'],
            'crm' => $rdata['crm'],
            'nascimento' => $rdata['nascimento'],
            'civil' => $rdata['civil'],
            'rg' => $rdata['rg'],
            'cpf' => $rdata['cpf'],
            'genero' => isset($rdata['genero']) ? $rdata['genero'] : "",
            'nacionalidade' => $rdata['nacionalidade'],
            'naturalidade' => $rdata['naturalidade'],
            'residencial' => $rdata['residencial'],
            'residencial_bairro' => $rdata['residencial_bairro'],
            'residencial_cep' => $rdata['residencial_cep'],
            'residencial_cidade' => $rdata['residencial_cidade'],
            'residencial_estado' => $rdata['residencial_estado'],
            'consultorio' => $rdata['consultorio'],
            'consultorio_bairro' => $rdata['consultorio_bairro'],
            'consultorio_cep' => $rdata['consultorio_cep'],
            'consultorio_cidade' => $rdata['consultorio_cidade'],
            'consultorio_estado' => $rdata['consultorio_estado'],
            'tel_residencial' => $rdata['tel_residencial'],
            'tel_consultorio' => $rdata['tel_consultorio'],
            'tel_celular' => $rdata['tel_celular'],
            'email' => $rdata['email'],
            'idioma' => $rdata['idioma'],
            'outro_idioma' => $rdata['outro_idioma'],
            'especialidade' => $rdata['especialidade'],
            'outra_especialidade' => $rdata['outra_especialidade'],
            'area_atuacao' => $rdata['area_atuacao'],
            'dados_carro' => $rdata['dados_carro'],
            'chefe_equipe' => $rdata['chefe_equipe'],
            'equipe' => isset($rdata['equipe']) ? $rdata['equipe'] : "",
            // FORMAÇÃO PROFISSIONAL
            'formacao_superior' => $rdata['formacao_superior'],
            'formacao_estado' => $rdata['formacao_estado'],
            'formacao_conclusao' => $rdata['formacao_conclusao'],
            'formacao_residencia' => $rdata['formacao_residencia'],
            'formacao_residencia_estado' => $rdata['formacao_residencia_estado'],
            'formacao_residencia_conclusao' => $rdata['formacao_residencia_conclusao'],
            'pos_lato_sensu' => isset($rdata['pos_lato_sensu']) ? $rdata['pos_lato_sensu'] : "não",
            'pos_mestrado' => isset($rdata['pos_mestrado']) ? $rdata['pos_mestrado'] : "não",
            'pos_doutorado' => isset($rdata['pos_doutorado']) ? $rdata['pos_doutorado'] : "não",
            'pos_livre_docencia' => isset($rdata['pos_livre_docencia']) ? $rdata['pos_livre_docencia'] : "não",
            'pos_instituicao' => $rdata['pos_instituicao'],
            'pos_instituicao_estado' => $rdata['pos_instituicao_estado'],
            'pos_instituicao_conclusao' => $rdata['pos_instituicao_conclusao'],
            // REFERÊNCIAS PROFISSIONAIS
            'ref1_nome' => $rdata['ref1_nome'],
            'ref1_crm' => $rdata['ref1_crm'],
            'ref1_instituicao' => $rdata['ref1_instituicao'],
            'ref1_funcao' => $rdata['ref1_funcao'],
            'ref1_especialidade' => $rdata['ref1_especialidade'],
            'ref1_email' => $rdata['ref1_email'],
            'ref1_tel_comercial' => $rdata['ref1_tel_comercial'],
            'ref1_tel_celular' => $rdata['ref1_tel_celular'],
            'ref2_nome' => $rdata['ref2_nome'],
            'ref2_crm' => $rdata['ref2_crm'],
            'ref2_instituicao' => $rdata['ref2_instituicao'],
            'ref2_funcao' => $rdata['ref2_funcao'],
            'ref2_especialidade' => $rdata['ref2_especialidade'],
            'ref2_email' => $rdata['ref2_email'],
            'ref2_tel_comercial' => $rdata['ref2_tel_comercial'],
            'ref2_tel_celular' => $rdata['ref2_tel_celular'],
            'ref3_nome' => $rdata['ref3_nome'],
            'ref3_crm' => $rdata['ref3_crm'],
            'ref3_instituicao' => $rdata['ref3_instituicao'],
            'ref3_funcao' => $rdata['ref3_funcao'],
            'ref3_especialidade' => $rdata['ref3_especialidade'],
            'ref3_email' => $rdata['ref3_email'],
            'ref3_tel_comercial' => $rdata['ref3_tel_comercial'],
            'ref3_tel_celular' => $rdata['ref3_tel_celular'],
            'ref4_nome' => $rdata['ref4_nome'],
            'ref4_crm' => $rdata['ref4_crm'],
            'ref4_instituicao' => $rdata['ref4_instituicao'],
            'ref4_funcao' => $rdata['ref4_funcao'],
            'ref4_especialidade' => $rdata['ref4_especialidade'],
            'ref4_email' => $rdata['ref4_email'],
            'ref4_tel_comercial' => $rdata['ref4_tel_comercial'],
            'ref4_tel_celular' => $rdata['ref4_tel_celular'],
            'ref5_nome' => $rdata['ref5_nome'],
            'ref5_crm' => $rdata['ref5_crm'],
            'ref5_instituicao' => $rdata['ref5_instituicao'],
            'ref5_funcao' => $rdata['ref5_funcao'],
            'ref5_especialidade' => $rdata['ref5_especialidade'],
            'ref5_email' => $rdata['ref5_email'],
            'ref5_tel_comercial' => $rdata['ref5_tel_comercial'],
            'ref5_tel_celular' => $rdata['ref5_tel_celular'],
            'sociedade_prof1' => $rdata['sociedade_prof1'],
            // LISTAR A(S) SOCIEDADE(S) PROFISSIONAL QUE É AFILIADO
            'sociedade_prof1_membro' => isset($rdata['sociedade_prof1_membro']) ? $rdata['sociedade_prof1_membro'] : "",
            'sociedade_prof1_admissao' => isset($rdata['sociedade_prof1_admissao']) ? $rdata['sociedade_prof1_admissao'] : "",
            'sociedade_prof2' => isset($rdata['sociedade_prof2']) ? $rdata['sociedade_prof2'] : "",
            'sociedade_prof2_membro' => isset($rdata['sociedade_prof2_membro']) ? $rdata['sociedade_prof2_membro'] : "",
            'sociedade_prof2_admissao' => isset($rdata['sociedade_prof2_admissao']) ? $rdata['sociedade_prof2_admissao'] : "",
            'sociedade_prof3' => isset($rdata['sociedade_prof3']) ? $rdata['sociedade_prof3'] : "",
            'sociedade_prof3_membro' => isset($rdata['sociedade_prof3_membro']) ? $rdata['sociedade_prof3_membro'] : "",
            'sociedade_prof3_admissao' => isset($rdata['sociedade_prof3_admissao']) ? $rdata['sociedade_prof3_admissao'] : "",
        );

        $msg = [
            'to' => env('MAIL_TO_ADDRESS', 'contato@csst.com.br'),
            'co' => env('MAIL_TO_NAME', 'Hospitais Next'),
            'sb' => 'Formulário de Cadastro Médico',
        ];

        Mail::send('emails.envio-cadastro-medico', ['heads' => $msg, 'fields' => $rdts], function ($m) use ($msg) {
            $m->to($msg['to'], $msg['co']);
            $m->subject($msg['sb'] . ' - ' . $msg['co']);
        });

        return response()->json([
            'success' => true,
            'message' => 'Obrigado, mensagem enviada!'
        ], 200);
    }
    */

    public function imprensa()
    {
        return view('imprensa');
    }

    public function contato()
    {
        return view('contato');
    }

    public function enviocontato(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'nome' => 'required|min:3|max:180',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 422);
        }

        // MZ: tentativa de criar um preview do template do email, veja mais e /routes/web.php
        // return Redirect::route('mail.preview', []);

        $msg = [
            'to' => env('MAIL_TO_ADDRESS', 'contato@csst.com.br'),
            'co' => env('MAIL_TO_NAME', 'Hospitais Next'),
            'sb' => 'Formulário de Contato',
            'nome' => $request->nome,
            'email' => $request->email,
            'assunto' => $request->assunto,
            'texto' => $request->texto,
        ];

        Mail::send('emails.envio-contato', ['fields' => $msg], function ($m) use ($msg) {
            $m->to($msg['to'], $msg['co']);
            $m->subject($msg['sb'] . ' - ' . $msg['co']);
        });

        return response()->json([
            'success' => true,
            'message' => 'Obrigado, mensagem enviada!'
        ], 200);
    }

    /**
     * Convert a comma separated file into an associated array.
     * The first row should contain the array keys.
     *
     * Example:
     *
     * @param string $filename Path to the CSV file
     * @param string $delimiter The separator used in the file
     * @return array
     * @link http://gist.github.com/385876
     * @author Jay Williams <http://myd3.com/>
     * @copyright Copyright (c) 2010, Jay Williams
     * @license http://www.opensource.org/licenses/mit-license.php MIT License
     */
    static public function csv_to_array($filename='', $delimiter=',')
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
