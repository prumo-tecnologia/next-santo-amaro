"use strict";
var gulp = require("gulp");
var sass = require('gulp-sass');

/**
 * Compile all the scss file and place in the build folder.
 */
gulp.task('sass', function () {
    gulp.src('./resources/assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css'));
});
// gulp.task("default", ['sass'], function () {
//     console.log("Building the project ...");
// });

gulp.task("default", gulp.series('sass', function () {
    console.log("Building the project ...");
}));